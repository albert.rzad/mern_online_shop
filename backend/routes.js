import express from 'express'
import userSchema from './Schematy/User.js'
import orderSchema from './Schematy/Order.js'
import bcrypt from 'bcrypt'
import mongoose from 'mongoose';
import cors from 'cors';
import jwt from 'jsonwebtoken';

const app = express();

app.use(express.json());


app.post('/rejestracja', async (req, res) => {
  const firstName = req.body.firstName;
  const lastName = req.body.lastName;
  const email = req.body.email;
  const password = req.body.password;
  
  const errors = {};

  if (!firstName) {
    errors.firstName = 'Pole imię jest wymagane.';
  } else if (!/^[A-Za-z]{3,}$/.test(firstName)) {
    errors.firstName = 'Imię musi składać się z minimum 3 liter i nie może zawierać cyfr ani znaków specjalnych.';
  }

  if (!lastName) {
    errors.lastName = 'Pole nazwisko jest wymagane.';
  } else if (!/^[A-Za-z]{3,}$/.test(lastName)) {
    errors.lastName = 'Nazwisko musi składać się z minimum 3 liter i nie może zawierać cyfr ani znaków specjalnych.';
  }

  if (!email) {
    errors.email = 'Pole email jest wymagane.';
  } else if (!/\S+@\S+\.\S+/.test(email)) {
    errors.email = 'Nieprawidłowy format adresu email.';
  }

  if (!password) {
    errors.password = 'Pole hasło jest wymagane.';
  } else if (password.length < 7) {
    errors.password = 'Hasło musi zawierać co najmniej 7 znaków.';
  } else if (!/\d/.test(password)) {
    errors.password = 'Hasło musi zawierać co najmniej jedną cyfrę.';
  }

  if (Object.keys(errors).length === 0) {
  
    mongoose.connect('mongodb+srv://albert_rzad:haslo123@labmongo.tifym9z.mongodb.net/?retryWrites=true&w=majority', {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    }).then(() => {
      console.log('Połączono z bazą danych');
    }).catch((error) => {
      console.error('Błąd podczas połączenia z bazą danych:', error);
    });

    const hashedPassword = await bcrypt.hash(password, 10);
  
    const User = mongoose.model('User', userSchema);
    const newUser = new User({
        firstName: firstName,
        lastName: lastName,
        email: email,
        password: hashedPassword,
      });

      const user = await User.findOne({"email": email});
    if (!user) {
      newUser.save()
      .then(() => {
        console.log("Zarejestrowano")
      })
      .catch((error) => {
        console.error('Błąd podczas rejestracji:', error);
   return  res.status(400).json({ message: "Użytkownik o podanym adresie email już istnieje." });
      });
    }
   return  res.status(400).json({ message: "Użytkownik o podanym adresie email już istnieje." });
  } else {
     res.status(400).json({ errors });
  }
});

app.post('/logowanie', async (req, res) => {
  const email = req.body.email;
  const password = req.body.password;
  const User = mongoose.model('User', userSchema);

  mongoose.connect('mongodb+srv://albert_rzad:haslo123@labmongo.tifym9z.mongodb.net/?retryWrites=true&w=majority', {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    }).then(() => {
      console.log('Połączono z bazą danych');
    }).catch((error) => {
      console.error('Błąd podczas połączenia z bazą danych:', error);
    });

  try {
    const user = await User.findOne({"email": email});
    if (!user) {
      return res.status(404).json({ message: 'Użytkownik o podanym adresie email nie istnieje' });
    }

    const passwordMatch = await bcrypt.compare(password, user.password)
    if (passwordMatch) {
      const token = jwt.sign({ _id: user._id }, user.password, {expiresIn: "7d",})
      return res.status(200).json({ data: {token,email}, message: "Zalogowano pomyślnie" })
    } else {
      return res.status(401).json({ error: 'Podane hasło jest nieprawidłowe.' });
    }
  } catch (error) {
    console.error('Błąd podczas logowania:', error);
    res.status(500).json({ message: 'Wystąpił błąd podczas logowania' });
  }
});

app.post('/wylogowywanie', async (req, res) => {
  res.redirect("stronaGlowna")
});

app.post('/zamowienie', async (req, res) => {
  const phoneNumber = req.body.phoneNumber;
  const email = req.body.email;
  const city = req.body.city;
  const street = req.body.street;
  const phone = req.body.phone;
  const payment = req.body.payment;

  const errors = {};

  if (!phoneNumber || phoneNumber.length !== 9 || !/^\d+$/.test(phoneNumber)) {
    errors.phoneNumber = 'Numer telefonu powinien składać się z dokładnie 9 cyfr.';
  }

  if (!city || !/^[A-Za-z]+$/.test(city)) {
    errors.city = 'Miasto powinno składać się tylko z liter.';
  }

  if (!street || !/^[A-Za-z]+\s\d+[A-Za-z]?$/.test(street)) {
    errors.street = 'Ulica powinna być w formacie "Nazwa ulicy + numer domu + ewentualna litera do numeru domu".';
  }

  if (!phoneNumber || !city || !street) {
    errors.general = 'Wszystkie pola formularza są wymagane.';
  }

  if (!email) {
    errors.email = 'Pole email jest wymagane.';
  } else if (!/\S+@\S+\.\S+/.test(email)) {
    errors.email = 'Nieprawidłowy format adresu email.';
  }

  if (Object.keys(errors).length === 0) {
  
    mongoose.connect('mongodb+srv://albert_rzad:haslo123@labmongo.tifym9z.mongodb.net/?retryWrites=true&w=majority', {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    }).then(() => {
      console.log('Połączono z bazą danych');
    }).catch((error) => {
      console.error('Błąd podczas połączenia z bazą danych:', error);
    });
  
    const Order = mongoose.model('Order', orderSchema);
    const newOrder = new Order({
      phoneNumber: phoneNumber,
      email: email,
      city: city,
      street: street,
      phone: phone,
      payment: payment
      });

      newOrder.save()
      .then(() => {
        res.status(200).json({ message: 'Zamówienie złożone' });
      })
      .catch((error) => {
        console.error('Błąd podczas realizacji zamówienia:', error);
        res.status(500).json({ message: 'Błąd podczas realizacji zamówienia' });
      });
  } else {
     res.status(400).json({ errors });
  }
});

app.get('/mojeZamowienia', async (req, res) => {

  try {
    mongoose.connect('mongodb+srv://albert_rzad:haslo123@labmongo.tifym9z.mongodb.net/?retryWrites=true&w=majority', {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    }).then(() => {
      console.log('Połączono z bazą danych');
    }).catch((error) => {
      console.error('Błąd podczas połączenia z bazą danych:', error);
    });

    const Order = mongoose.model('Order', orderSchema);
    const orders = await Order.find({ email: req.query.email }); // Pobieranie wszystkich dokumentów z kolekcji "orders"
    res.json(orders);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Wystąpił błąd podczas odczytu zamówień.' });
  }
});

app.delete('/zamowienia/:id', async (req, res) => {
  const orderId = req.params.id;

  try {
    const Order = mongoose.model('Order', orderSchema);

    mongoose.connect('mongodb+srv://albert_rzad:haslo123@labmongo.tifym9z.mongodb.net/?retryWrites=true&w=majority', {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    })

    await Order.findByIdAndRemove(orderId);

    res.status(200).json({ message: 'Zamówienie usunięte' });
  } catch (error) {
    console.error('Error deleting order:', error);
    res.status(500).json({ message: 'Błąd podczas usuwania zamówienia' });
  } 
});

app.put('/zmianaHasla', async (req, res) => {
  const actualPassword = req.body.actualPassword
  const newPassword = req.body.newPassword
  const hashedNewPassword = await bcrypt.hash(newPassword, 10);

  mongoose.connect('mongodb+srv://albert_rzad:haslo123@labmongo.tifym9z.mongodb.net/?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }).then(() => {
    console.log('Połączono z bazą danych');
  }).catch((error) => {
    console.error('Błąd podczas połączenia z bazą danych:', error);
  });

  const User = mongoose.model('User', userSchema);
  const userModelToUpdate = await User.findOne({email: req.query.email});
  const isPasswordMatch =  bcrypt.compare(actualPassword, userModelToUpdate.password);
  console.log(isPasswordMatch.value);
  if (!isPasswordMatch) {
    return res.status(401).json({ message: 'Niepoprawne hasło' });
  }
  try {
    const updatedUser = await User.findByIdAndUpdate(
      userModelToUpdate._id.toString(),
      { password: hashedNewPassword },
      { new: true }
    );
    await updatedUser.save();
    if (!updatedUser) {
      return res.status(404).json({ message: '' });
    }

    res.status(200).json({ message: 'Haslo zmienione'});
  } catch (error) {
    console.error('Error updating password:', error);
    res.status(500).json({ message: 'Błąd podczas aktualizowania hasła' });
  }
});


app.listen(8080, () => {
  console.log('Serwer nasłuchuje na porcie 8080');
});


